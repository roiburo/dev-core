import './pug/pages/index.pug'

//components
import components from "./js/components/components";

// components
import MainPageCarousel from './js/components/carousels/mainPageCarousel'
import 'bootstrap';
import 'popper.js';
import 'owl.carousel/dist/assets/owl.carousel.css';
import '@fancyapps/fancybox/dist/jquery.fancybox.min.css';
import 'owl.carousel/dist/owl.carousel.min'

//validation
import FormValidations from "./js/validation/formValidations";

import css from "./style.scss";

//carousels
import NewsCarousel from './js/components/carousels/carousel-news'

//initPages
import InitPages from './js/init'

InitPages();

components();
MainPageCarousel();
NewsCarousel();
FormValidations();

    function requireAll(r) {
    r.keys().forEach(r);
}

requireAll(require.context('./assets/svg/', true, /\.svg$/));
