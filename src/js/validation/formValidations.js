import Bouncer from 'formbouncerjs/dist/bouncer.polyfills.min'
import Mask from 'imask/dist/imask.min'

export default function () {
    let phoneInput = document.getElementById('phone');
    let phoneOptions = {
        mask: '+{7}(000) 000-00-00'
    };

    let maskPhone = Mask(phoneInput, phoneOptions);
    let validate = new Bouncer('form', {
        messages: {
            missingValue: {
                checkbox: 'This field is required.',
                radio: 'Please select a value.',
                select: 'Please select a value.',
                'select-multiple': 'Please select at least one value.',
                default: 'Поле не заполнено'
            },
            patternMismatch: {
                email: 'Неверный email',
                url: 'Please enter a URL.',
                number: 'Please enter a number',
                color: 'Please match the following format: #rrggbb',
                date: 'Please use the YYYY-MM-DD format',
                time: 'Please use the 24-hour time format. Ex. 23:00',
                month: 'Please use the YYYY-MM format',
                default: 'Please match the requested format.'
            },
            outOfRange: {
                over: 'Введите больше {max} символов.',
                under: 'Введите не более {min} символов.'
            },
            wrongLength: {
                over: 'Введите корректные данные',
                under: 'Введите корректные данные'
            }
        },
    });


}