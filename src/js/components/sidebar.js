import accordion from '../components/accordion';

export default function () {
    let menubtn = document.querySelector('.header-menu');
    let sidebar = document.querySelector('.sidebar');
    let sidePanel = document.querySelector('.js-sb')
    let accordionsInn = document.querySelectorAll('.accordion-inn');
    let accordionsLast = document.querySelectorAll('.accordion-last');

    // let accordionsLast = document.querySelectorAll('.accordion-inn__header');

    let accordionsLastHeader = document.querySelectorAll('.accordion-last__header');

    let sidePanels = sidebar.querySelectorAll('.last-menu');
    // let panelBtns = sidebar.querySelectorAll('.js-sidebar');
    let closePanelBtns = sidebar.querySelectorAll('.js-close-lastmenu');
    // let accordions = sidebar.querySelectorAll('.js-acc');
    // let btns = sidebar.querySelectorAll('.js-acc-btn');
    // let list = sidebar.querySelectorAll('.js-acc-inn');


    let showSidebar = () => {
        sidePanel.classList.toggle('active');
    };

    menubtn.addEventListener('click', function () {
        showSidebar();
    })

    let resetAccordionsInnStyles = (id) => {
        accordionsInn.forEach((el, index) => {
            if (el.classList.contains('active') && id !== index) {
                el.classList.toggle('active');
            }
        })
    }

    accordionsInn.forEach((el, index) => {
        el.addEventListener('click', function () {
            resetAccordionsInnStyles(index);
            el.classList.toggle('active');
        })
    })

    let resetAccordionsLastHeaderStyles = () => {
        accordionsLastHeader.forEach(el => {
            if (el.classList.contains('active')) {
                el.classList.remove('active');
            }
        })
    }

    accordionsLastHeader.forEach(el => {
        el.addEventListener('click', function () {
            resetAccordionsLastHeaderStyles();
            el.classList.toggle('active');
        })
    })

    // show sidePanel by index when accordionBtn clicked
    let showSidePanel = index => {
        sidePanels[index].classList.toggle('last-menu--active')
    }

    //addEventListener to sidePanelsBtns
    accordionsLast.forEach((el, index) => {
        let btn = el.querySelector('.accordion-last__icon');
        btn.addEventListener('click', function () {
            showSidePanel(index)
        })
    })

    let closeSidePanel = closePanelBtns.forEach((el, index) => {
        el.addEventListener('click', () => {
            sidePanels[index].classList.toggle('last-menu--active')
        })
    })


    // let resetAccordions = (elem, match, appendClass) => {
    //     elem.forEach((el, index) => {
    //         if (index !== match) {
    //             el.style.maxHeight = null;
    //             if (el.classList.contains(appendClass)) {
    //                 el.classList.toggle(appendClass);
    //             }
    //         }
    //     })
    // }
    //
    // let activeAccordions = () => {
    //
    //     for (let i = 0; i <= btns.length - 1; i++) {
    //         btns[i].addEventListener('click', function () {
    //             resetAccordions(list, i);
    //             resetAccordions(btns, i, 'sb-selector__icon--active');
    //             resetAccordions(accordions, i, 'accord-active');
    //             setTimeout(() => {
    //                 accordion(list[i])
    //             }, 150)
    //
    //             if (btns[i].classList.contains('sb-selector__icon')) {
    //                 btns[i].classList.toggle('sb-selector__icon--active')
    //             } else {
    //                 setTimeout(() => {
    //                     accordions[i].classList.toggle('accord-active')
    //                 }, 150)
    //             }
    //         })
    //     }
    // }
    //

}



