import showAccordion from '../components/accordion';

export default function () {
    let items = document.querySelector('.footer__items');
    let btns = items.querySelectorAll('.js-acc-btn');
    let inner = items.querySelectorAll('.list-inn');

    let resetAccordions = (elem, match, appendClass) => {
        elem.forEach((el, index) => {
            if (index !== match) {
                el.style.maxHeight = null;
                if (el.classList.contains(appendClass)) {
                    el.classList.toggle(appendClass);
                }
            }
        })
    }

    btns.forEach((el, i) => {
        el.addEventListener('click', function () {
            el.classList.toggle('accordion__icon--active')
            resetAccordions(btns, i, "accordion__icon--active")
            resetAccordions(inner, i)
            showAccordion(inner[i])
        })
    })




}