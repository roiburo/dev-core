import ProgressBar from 'progressbar.js'

export default function (shapeId, textId, color, progress ) {
    // finance circle bar
    let progressText = document.querySelector(textId);
    let bar = new ProgressBar.Circle(shapeId, {
        color: '#aaa',
        // This has to be the same size as the maximum width to
        // prevent clipping
        strokeWidth: 19,
        trailWidth: 19,
        easing: 'easeInOut',
        duration: 1400,
        text: {
            autoStyleContainer: false
        },
        from: { color: '#aaa', width: 19 },
        to: { color: color, width: 19 },
        // Set default step function for all animate calls
        step: function(state, circle) {
            circle.path.setAttribute('stroke', state.color);
            circle.path.setAttribute('stroke-width', state.width);

            let value = Math.round(circle.value() * 100);
            if (value === 0) {
                circle.setText('');
            } else {
                progressText.innerHTML = value;
            }
        }
    });
    bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
    bar.text.style.fontSize = '2rem';
    bar.animate(progress);  // Number from 0.0 to 1.0
}

