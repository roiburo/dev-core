import chart from "../components/graphs/chart";
import ProgressRing from "../components/progress-ring";
import multiCharts from '../components/graphs/multiCharts'


export default function () {
    const chartCapital = {
        node: '#capital',
        color: 'F2B61A',
        period: [
        "2013 год",
        "2014 год",
        "2015 год",
        "2016 год",
        "2017 год",
        "2018 год",
        "2019 год"
        ],
        values: [0, 140000, 260000, 300000, 360000, 420000, 500000],
        gradients: {
            from: 'rgba(242, 182, 26, 0.55)',
            to: 'rgba(239, 232, 213, 0)'
        }
    }

    const chartProfit = {
        node: '#profit',
        color: '#2FCDCA',
        period: [
            "2013 год",
            "2014 год",
            "2015 год",
            "2016 год",
            "2017 год",
            "2018 год",
            "2019 год"
        ],
        values: [0, 140000, 260000, 300000, 360000, 420000, 500000],
        gradients: {
            from: '#2FCDCA',
            to: 'rgba(239, 232, 213, 0)'
        }
    }

    const chartBalance = {
        node: '#balance',
        color: '#2D9CDB',
        period: [
            "2013 год",
            "2014 год",
            "2015 год",
            "2016 год",
            "2017 год",
            "2018 год",
            "2019 год"
        ],
        values: [0, 140000, 260000, 300000, 360000, 420000, 500000],
        gradients: {
            from: '#4BB6F3',
            to: 'rgba(237, 242, 244, 0)'
        }
    }

    const chartRevenue = {
        node: '#revenue',
        color: '#27AE60',
        period: [
            "2013 год",
            "2014 год",
            "2015 год",
            "2016 год",
            "2017 год",
            "2018 год",
            "2019 год"
        ],
        values: [0, 140000, 260000, 300000, 360000, 420000, 500000],
        gradients: {
            from: '#47C77D',
            to: 'rgba(196, 196, 196, 0)'
        }
    }

    //initialize charts
    chart(chartCapital.node, chartCapital.color, chartCapital.period, chartCapital.values, chartCapital.gradients )
    chart(chartProfit.node, chartProfit.color, chartProfit.period, chartProfit.values, chartProfit.gradients )
    chart(chartBalance.node, chartBalance.color, chartBalance.period, chartBalance.values, chartBalance.gradients )
    chart(chartRevenue.node, chartRevenue.color, chartRevenue.period, chartRevenue.values, chartRevenue.gradients )


    ProgressRing('#finance', '.js-finance', '#27AE60', 0.88);
    ProgressRing('#guarantee', '.js-guarantee', '#2D9CDB', 0.98);
    multiCharts();

}



