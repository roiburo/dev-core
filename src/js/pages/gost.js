export default function () {
    let parent = document.querySelector('.gost');
    if (!parent) {
        return
    }
    let btn = parent.querySelector('.js-btn');
    let gosts = parent.querySelector('.gost__cards');
    btn.addEventListener('click', function () {
        accordionHide(gosts);
    })

    let accordionHide = (el) => {
        if (el.style.maxHeight) {
            el.style.maxHeight = null;
        } else {
            el.style.maxHeight = el.scrollHeight + "px";
        }
    };
}



