import '@fancyapps/fancybox/dist/jquery.fancybox.min'

export default function () {
    let galleryItems = document.querySelectorAll('.js-item');
    let sortedImages = [];
    let carouselJq = $('.owl-carousel-ss');
    let btns = document.querySelectorAll('.control__img');

    carouselJq.owlCarousel({
        autoHeight: true,
        margin: 10,
        items: 1,
        dots: true,
        touchDrag: false,
        mouseDrag: false,
        loop: true,
        // autoplay: true,
    })

    // take images from carousel and apply to sortedImages
    let sortImages = () => {
        galleryItems.forEach(img => {
            sortedImages.push({
                src: img.getAttribute('src'),
                opts: {
                    thumb: img.getAttribute('src')
                }
            })
        })
    }
    sortImages();

    // change owlCarousel on selected thumb button image
    btns.forEach((btn, id) => {
        btn.addEventListener('click', function () {
            btns.forEach(el => {
                el.classList.remove('active');
            });
            btn.classList.add('active');
            carouselJq.trigger('to.owl.carousel', id);
        })
    });

    // generate event for every shows carousel item to open fancybox
    galleryItems.forEach( (panel, index) => {
        panel.addEventListener('click', () => {
            console.log(panel);
            $.fancybox.open(sortedImages, '', index);
        })
    });
}
