
export default function () {
    $('.special-offers-carousel').owlCarousel({
        margin: 17,
        responsiveClass: true,
        nav: true,
        navText: ['<svg class="news-images-nav-prev">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>', '<svg class="news-images-nav-next" ">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>'],
        responsive: {
            0: {
                items: 1,
            },
            700: {
                items: 2,
            },
            900: {
              items: 3,
              dots: true
            },
            1300: {
                items: 4,
            }
        }
    })
}



