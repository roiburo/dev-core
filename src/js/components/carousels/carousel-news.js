import showAccordion from "../accordion";

export default function () {
    let btns = document.querySelectorAll('.cheap');

    btns.forEach(el => {
        el.addEventListener('click', function () {
            resetLists();
            el.classList.add('active')
        })
    });

    let resetLists = () => {
        btns.forEach(el => {
            el.classList.remove('active')
        })
    };

    $('.news-carousel').owlCarousel({
        autoHeight: true,
        margin: 10,
        responsiveClass: true,
        nav: true,
        dots: true,
        navText: ['<svg class="specials-nav-prev">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>', '<svg class="specials-nav-next" ">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>'],
        responsive: {
            0: {
                items: 1,
            },
            700: {
                items: 2,
            },
            900: {
                dots: false,
                items: 3
            }
        }
    })
}