export default function () {
    $('.special-carousel-offers').owlCarousel({
        autoHeight: true,
        margin: 10,
        responsiveClass: true,
        nav: true,
        loop: true,
        autoplay: true,
        dots: true,
        navText: ['<svg class="specials-nav-prev">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>', '<svg class="specials-nav-next" ">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>'],
        responsive: {
            0: {
                items: 1,
            },
            700: {
                items: 2,
            },
            900: {
                nav: false,
                dots: false,
                items: 3
            }
        }
    })
}