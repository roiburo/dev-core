import Chart from "chart.js/dist/Chart.bundle";

export default function () {
    let graphDeals = document.getElementById('deals').getContext("2d")
    let graphContracts = document.getElementById('contracts').getContext("2d")
    // graphs gradients

    let gradientDealsOne = graphDeals.createLinearGradient(0, 0, 0, 340);
    gradientDealsOne.addColorStop(0, "#2D9CDB");
    gradientDealsOne.addColorStop(1, "rgba(255, 255, 255, 0)");
    let gradientDealsTwo = graphDeals.createLinearGradient(0, 0, 0, 110);
    gradientDealsTwo.addColorStop(0, "#F2B61A");
    gradientDealsTwo.addColorStop(1, "rgba(255, 255, 255, 0)");
    let gradientContracts = graphContracts.createLinearGradient(0, 0, 0, 110);
    gradientContracts.addColorStop(0, "#2D9CDB");
    gradientContracts.addColorStop(1, "rgba(255, 255, 255, 0)");

    let chartDeals = new Chart(graphDeals, {
        type: 'line',
        data: {
            labels: ["2014", "2015", "2016", "2017", "2018", "2019"],
            datasets: [{
                borderColor: "#2D9CDB",
                pointBorderColor: "#2D9CDB",
                pointBackgroundColor: "#2D9CDB",
                pointHoverBackgroundColor: "#2D9CDB",
                pointHoverBorderColor: "#2D9CDB",
                fill: true,
                backgroundColor: gradientDealsOne,
                borderWidth: 2,
                data: [0, 3, 0, 0, 0, 0 ]
            },
                {
                    borderColor: "#F2B61A",
                    pointBorderColor: "#F2B61A",
                    pointBackgroundColor: "#F2B61A",
                    pointHoverBackgroundColor: "#F2B61A",
                    pointHoverBorderColor: "#F2B61A",
                    fill: true,
                    backgroundColor: gradientDealsTwo,
                    borderWidth: 2,
                    data: [1, 1, 1, 6, 12, 0]
                }]
        },
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        drawTicks: false,
                        display: false
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        padding: 24,
                        border: '1px solid black',
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "300",
                        lineHeight: "16px",
                        fontSize: "12",
                        fontWeight: "300",
                    }
                }]
            }
        }
    });

    let chartContracts = new Chart(graphContracts, {
        type: 'line',
        data: {
            labels: ["2014", "2015", "2016", "2017", "2018", "2019"],
            datasets: [{
                borderColor: "#47C77D",
                pointBorderColor: "#47C77D",
                pointBackgroundColor: "#47C77D",
                pointHoverBackgroundColor: "#47C77D",
                pointHoverBorderColor: "#47C77D",
                fill: false,
                borderWidth: 2,
                data: [0, 0, 0, 0, 0, 0 ]
            },
                {
                    borderColor: "#2D9CDB",
                    pointBorderColor: "#2D9CDB",
                    pointBackgroundColor: "#2D9CDB",
                    pointHoverBackgroundColor: "#2D9CDB",
                    pointHoverBorderColor: "#2D9CDB",
                    fill: true,
                    backgroundColor: gradientContracts,
                    borderWidth: 2,
                    data: [1, 1, 9, 81, 38, 0]
                }]
        },
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        drawTicks: false,
                        display: false
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        padding: 24,
                        border: '1px solid black',
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "300",
                        lineHeight: "16px",
                        fontSize: "12",
                        fontWeight: "300",
                    }
                }]
            }
        }
    });
}