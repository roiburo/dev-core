import ApexCharts from 'apexcharts'

export default function(element ,color, period, values, gradients) {
    var options = {

        grid: {
            show: false
        },
        stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: [color],
            width: 2,
            dashArray: 0
        },
        chart: {
            height: 110,
            type: "area",
            animations: {
                enabled: false,
            },
            toolbar: {
                show: false
            }
        },

        markers: {
            size: 0,
            colors: [color],
            strokeColor: "rgba(242, 182, 26, .55)",
            strokeWidth: 6,
            hover: {
                size: 4,
            }
        },
        dataLabels: {
            enabled: false
        },
        series: [
            {
                name: "Прибыль",
                data: values,
            }
        ],
        fill : {
            type: 'gradient',

            gradient: {
                shade: 'light',
                opacityFrom: .55,
                opacityTo: .55,
                stops: [100, 50, 0],
                colorStops: [
                    [
                        {
                            offset: 0,
                            color: gradients.from,
                            opacity: 1
                        },
                        {
                            offset: 100,
                            color: gradients.to,
                            opacity: 1
                        }
                    ],
                ]

            }
        },
        yaxis: {
            labels: {
                show: false
            }
        },
        xaxis: {
            categories: period,
            labels: {
                show: false
            }
        }
    };

    var chart = new ApexCharts(document.querySelector(element), options);

    chart.render();
}