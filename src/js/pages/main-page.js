import specials from '../components/carousels/special-offers';
import Gallery from '../components/gallery-docs';


export default function () {
let parent = document.querySelector('.sb-slava');
// let btns = parent.querySelectorAll('.js-btn');
// let lists = parent.querySelectorAll('.panel');
// let accordion = parent.querySelector('.accordions');
// let panelIndex =  parent.querySelector('.panel-index');
// a bottom part of card

    // btns.forEach((button, i) => {
    //     button.addEventListener('click', function () {
    //         accordion.classList.toggle('accordion-hide')
    //         setTimeout(() => {
    //             resetLists(i)
    //             setValues(button)
    //             accordionHide(lists[i]);
    //             accordion.classList.toggle('accordion-hide')
    //         }, 500);
    //     })
    // });

    // let resetLists = (id) => {
    //     lists.forEach((list, index) => {
    //         if (id == index) {
    //             return
    //         }
    //         list.style.maxHeight = 0;
    //     })
    // }

    // set checked values to panel index
    // let setValues = (el) => {
    //     let text = panelIndex.querySelector('.text');
    //     let svg = panelIndex.querySelector('use');
    //     let svgSrc = el.querySelector('use').attributes[0].value;
    //     svg.setAttributeNS('http://www.w3.org/1999/xlink', 'href', svgSrc);
    //     text.innerHTML = el.querySelector('.text').innerHTML;
    //     // let icon = el.querySelector('.text').innerHTML;
    // };

    // let accordionHide = (el) => {
    //     if (el.style.maxHeight) {
    //         el.style.maxHeight = null;
    //     } else {
    //         el.style.maxHeight = el.scrollHeight + "px";
    //     }
    // };

    // carousel statistick
    $('.stat-carousel').owlCarousel({
        autoHeight: true,
        margin: 10,
        responsiveClass: true,
        nav: true,
        dots: true,
        navText: ['<svg class="specials-nav-prev">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>', '<svg class="specials-nav-next" ">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>'],
        responsive: {
            0: {
                items: 1,
            },
            700: {
                items: 2,
            },
            1200: {
                items: 3,
                nav: false,
            }
        }
    })

    $('.special-offers-carousel').owlCarousel({
        autoHeight: true,
        margin: 10,
        responsiveClass: true,
        nav: true,
        dots: true,
        navText: ['<svg class="specials-nav-prev">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>', '<svg class="specials-nav-next" ">\n' +
        '  <use xlink:href="/src/assets/icons.svg#arrow-down"></use>\n' +
        '</svg>'],
        responsive: {
            0: {
                items: 1,
            },
            700: {
                items: 2,
            },
            1200: {
                items: 3,
                nav: false,
            }
        }
    })
    specials()
    Gallery()
}



