import CarouselServices from '../components/carousels/carousel-services';

export default function () {
    let selectedLabel = document.querySelector('#label');
    let labels = document.querySelectorAll('.js-label');

    labels.forEach( label => {
        label.addEventListener('click', function () {
            resetLists();
            label.classList.add('active');
            selectedLabel.innerHTML = label.innerHTML
        })
    });

    let resetLists = () => {
        labels.forEach((label, index) => {
            label.classList.remove('active');
        })
    };

    // first show marks accordion
    $('#collapse').collapse({
        toggle: true
    })

    CarouselServices()


}



