import indexPage from './pages/main-page'
import PortfolioDetailed from './pages/portfolio-detailed'
import StatisticPage from './pages/statistic'
import SpecialsList from './pages/specials-list'
import SpecialDetailed from './pages/special-detailed'
import Product from './pages/product-page'
import News from './pages/news'
import Gost from './pages/gost'


export default function () {
    let page = document.querySelector('.page-content')
    if (!page.hasAttribute('data-page')) {
        return;
    }
    let pageId = page.getAttribute('data-page');
    console.log(pageId);
    switch (pageId) {
        case 'index-page':
            indexPage();
            break;
        case 'portfolio-detailed':
            PortfolioDetailed();
            break;
        case 'statistics':
            StatisticPage();
            break;
        case 'specials-list':
            SpecialsList();
            break;
        case 'special-detailed':
            SpecialDetailed();
            break;
        case 'product':
            Product();
            break;
        case 'news':
            News();
            break;
        case 'gost':
            Gost();
            break;
        default:
            console.log('page is not exists');
    }
}