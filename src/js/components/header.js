export default function () {
    window.onscroll = function () {
        pinHeader()
    };

// Get the header
    let header = document.querySelector(".header");

// Get the offset position of the navbar
    let sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position

    let pinHeader = () => {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
}

