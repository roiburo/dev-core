const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const PATHS = {
    src: path.join(__dirname, './src'),
    dist: path.join(__dirname, './dist'),
    assets: 'assets/'
}

const PAGES_DIR = `${PATHS.src}/pug/pages/`
const PAGES = fs.readdirSync(PAGES_DIR).filter(fileName => fileName.endsWith('.pug'))


module.exports = {
    mode: 'development',
    // devtool: 'source-map',
    entry: {main: './src/main.js'},
    // entry: {main: `${PATHS.src}/main.js`},
    externals: {
        paths: PATHS
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    devServer: {
        hot: true,
        host:
            'localhost',
        port:
            8080,
        watchOptions:
            {
                poll: true
            }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            // sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            // sourceMap: true,
                            // sassOptions: {
                            //     outputStyle: 'compressed',
                            // },
                        }
                    }
                ]
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.pug$/,
                loader: 'pug-loader'
            },
            {
                test: /.svg/,
                exclude: /node_modules/,
                use:
                    [
                        {
                            loader: 'svg-sprite-loader',
                            options: {
                                extract: true,
                                spriteFilename: './src/assets/icons.svg', // this is the destination of your sprite sheet
                                runtimeCompat: true
                            }

                        },
                    ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "build.css",
        }),
        // build all pages, too long
        ...PAGES.map(page => new HtmlWebpackPlugin({
            template: `${PAGES_DIR}/${page}`,
            filename: `./${page.replace(/\.pug/, '.html')}`,
        })),
        // build only one page to enhance time spending
        // new HtmlWebpackPlugin({
        //     template: `${PATHS.src}/pug/pages/express-analysis.pug`,
        //     filename: 'express-analysis.html',
        // }),
        new LiveReloadPlugin({appendScriptTag: true}),
        new SpriteLoaderPlugin({
            plainSprite: true
        }),
        new CopyWebpackPlugin([{
            from: './src/assets/',
            to: 'src/assets'
        }]),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
    ]
}