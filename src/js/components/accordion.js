export default function (elem) {
    if (!elem.style.maxHeight) {
        elem.style.maxHeight = elem.scrollHeight + "px";
    } else {
        elem.style.maxHeight = null;
    }
}

