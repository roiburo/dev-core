import '@fancyapps/fancybox/dist/jquery.fancybox.min'

export default function () {
    let images = document.querySelectorAll('.js-item');
    let carouselJq = $('.owl-carousel-portfolio');
    let btns = document.querySelector('.gallery-cards').querySelectorAll('img');
    let sortedImages = [];


    carouselJq.owlCarousel({
        autoHeight: true,
        margin: 10,
        items: 1,
        dots: true,
        dotsData: true,
        mouseDrag: false,
        autoplay: true,
        loop: true,
    })

    let boxImages = document.querySelectorAll('.owl-item');

    let sortImages = () => {
        images.forEach(img => {
            sortedImages.push({
                src: img.getAttribute('src'),
                opts: {
                    thumb: img.getAttribute('src')
                }
            })
        })
    };
    sortImages();


    btns.forEach((btn, id) => {
        btn.addEventListener('click', function (e) {
            e.preventDefault();
            carouselJq.trigger('to.owl.carousel', id);
        })
    });

    // generate event for every shows carousel item to open fancybox
    boxImages.forEach((panel) => {
        panel.addEventListener('click', () => {
            let selectedBoxImage = null;
            boxImages.forEach( el => {
                if (el.classList.contains('active')) {
                    selectedBoxImage = el;
                }
            });
            let matchImages = sortedImages.findIndex( el => selectedBoxImage.querySelector('img').getAttribute('src') === el.src);
            $.fancybox.open(sortedImages, '', matchImages)
        })
    });
}



