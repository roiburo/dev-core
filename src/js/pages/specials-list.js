export default function () {
    let parent = document.querySelector('.specials-list');
    let btns = parent.querySelectorAll('.card-sl__more');
    let lists = parent.querySelectorAll('.list');
    // a bottom part of card
    let panelsAccordion = parent.querySelectorAll('.card-sl__content');

    btns.forEach((button, i) => {
        button.addEventListener('click', function (e) {
            resetLists(i)
            setTimeout(() => {
                resetPanelsAccordion(i)
            }, 300)
            accordion(lists[i])
            // use settimeout when card slapped that container "specials-list__cards"  will not expand
            if (panelsAccordion[i].classList.contains('active')) {
                setTimeout(() => {
                    panelsAccordion[i].classList.remove('active')
                }, 300)
            } else {
                panelsAccordion[i].classList.add('active')
            }
        })
    })

    // reset all cards excluding activated card
    let resetLists = (id) => {
        lists.forEach((list, index) => {
            if (id == index) {
                return
            }
            list.style.maxHeight = null;
        })
    }

    let resetPanelsAccordion = (id) => {
        panelsAccordion.forEach((panel, index) => {
            if (id == index) {
                return
            }
            panel.classList.remove('active')
        })
    }

    let accordion = (el) => {
        if (el.style.maxHeight) {
            el.style.maxHeight = null;
        } else {
            el.style.maxHeight = el.scrollHeight + "px";
        }
    };
}



